##Yuancheng Tu, Dan Roth, "Learning English Light Verb Constructions: Contextual or Statistical"
###Abstract
* 教師あり機械学習を用いた英語LVCの学習について．
* 提案システムは86.3%・ベースラインは52.2%(Accuracy), 文脈or統計的素性を用いて．
* どちらの提案素性も経験的には同じような性能になったが，全く同一の表層形のLVCの候補については，文脈素性を用いたほうが16.7%良かった．
* データセット: 均衡英語コーパス(2162文のLVCを含む文・British Natural Corpusから抽出)
    * このデータセットは公開されている．
    * Multi Word Expression のリソースとしては一般的に有用である．


##Matthew R. Scott, Xiaohua Liu, Ming Zhou, Microsoft Engkoo Team, "Engkoo: Mining the Web for Language Learning"
###Abstract
* Engkooという言語研究および学習のシステムを提案する．
    * これは主に数十億のウェブページの翻訳知識からマイニングした．(インターネットを使うことで実世界の言語をとらえる)
    * 今のところ中国人ESLのためのものであるが，技術自体は言語非依存である．
* Engkooは多様なNLP技術に基づくアプリケーションプラットフォームである．
    * 言語間情報抽出・アラインメント・文分類・SMT
    * 大規模な対訳データに基づく．
    * 特に英中どちらも含んでいるページはその対訳性について解析され用語の定義や例文として抽出される．
* この手法は最大規模の英中間の語彙的関係を構築することができる．
* またネット上で常にアップデートされる用語をカバーできる．

##Michael Gamon, Claudia Leacock, "Search right and thou shalt find... Using Web Queries for Learner Error Detection", ACL HLT 2010
###Abstract
* Non-native Writingの誤りをウェブ検索クエリを用いて検出する試み．
* 学習者が書いた文の中から正しい部分を区別することが基本タスク．
* アノテーション済みの学習者コーパスを用いて，ウェブ検索の頻度情報に基づくこの手法がこのタスクに有効かどうか評価した．
    * 評価においてはクエリ形成のための多様な手法と，ウェブ資源(Google API，Bing API，ウェブベースのngram)を用いた

###Introduction
データ駆動の手法によるNon native英語の誤り検出・訂正はここ数年で活発に研究されている．

##Native Judgements of Non-Native Usage: Experiments in Preposition Error Detection
###Abstract
* 評価とアノテーションはNon nativeのwritingの文法や語法誤りを教育したり判定するNLPツールの開発時には，最大の問題である．
* 既存の手法ではシステム出力と比較するための学習者コーパスのRaterはただ一人であることが一般的である．
* 本論文では一人のRaterのみに頼ることによりどれだけシステムの評価が偏るかを示し，そして，よりシステムの評価を効率的にするサンプリングを用いた手法を提案する．

##Raghavendra Udupa, Mitesh Khapra, "Improving the Multilingual User Experience of Wikipedia Using Cross-Language Name Search", NAACL2010
###Abstract
* Wikipediaはウェブ上の協調的でパワフルな百科事典ではあるが，Multilingualな部分は限られており，ほとんどのコンテンツは英語か少数の言語である．
* 実世界の一般的な状況; 非英語圏やESLのユーザーは，彼らの言語の記事がないゆえに英語の関連記事を探す必要がある．
* このようなユーザーのUXは，彼らが必要とする情報を第一言語で英語のWikipediaでの検索時に表現できれば著しく向上できるだろう．
* 本研究では...
    * Cross-Languageの名前検索アルゴリズムを提案する
    * 提案手法を，Hebrew, Hindi, Russian, Kannada, Bangla, Tamil語と英語間での記事検索に用いた
* 以上の経験的研究では，本手法によりUXは向上することがわかった．

##Helen Yannakoudakis, Ted Briscoe, Ben Medlock, "A New Dataset and Method for Automatically Grading ESOL Texts, ACL2011
###Abstract
* 本論文では教師あり機械学習の技法がESOLの試験作文の採点・評価に利用できるかを示す．
* 特に，本論文ではランキング学習を作文間のGrade関係を明示的にモデル化するために用いた．
* 多数の素性を抽出する．それらが全体にどれぐらい寄与するかを測るために部分的なテストを行った．
* 回帰と提案手法との比較の結果，提案手法のほうがずっと良かった．
* 公開されているデータセットに対する実験結果は，人手による採点というタスクの上限に迫る性能を達成できることがわかる．
* 最終的には部外者の文を用いてモデルの有効性をテストした．
* また，モデルの出すスコアが人手で付けられたスコアと乖離する場合を特定した．
