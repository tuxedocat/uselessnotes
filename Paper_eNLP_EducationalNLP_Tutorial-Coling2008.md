latex input:        mmd-article-header  
Title:                  Coling2008 Tutorial: "Educational Natural Language Processing"
Date:                 
Author:               
Base Header Level:  2    
LaTeX Mode:         memoir   
latex input:        mmd-article-begin-doc  
latex footer:       mmd-memoir-footer  

##Automatic Question Generation
空欄を埋めるような試験問題を自動で生成するというタスクを例にして...  
キーの生成・stemの生成・Distractorsの生成

>正解以外の選択肢: Distractors

これを自動で生成する．簡単な場合だと同じPOSタグであるとか．しかしながら，distractorが正解の選択肢に近すぎるとだめ．もちろん遠いのは問題としてだめ． 
意味的な類似度とかシソーラスを用いたものが最近では提案されている．

Distractorの評価には，その問題文との共起が極端に起こりにくいことを示す方法がある．また，Webでそのdistractorを含む問題文の頻度が高ければそれはdistractorとして機能しないので棄却する，とか．

この他にもすべての文の空欄にはまる尤もらしい単語を選ぶ問題とか，文中の誤り箇所を指摘する問題がある．
###自動生成された問題の評価について
>学生による評価・教員による評価

つまり，難しさ: 解答にかかる時間と難易度・使いやすさ: 後での手直しの必要性などである．

以下割愛．

##Assessment of learner generated discourse
言語学習者による談話生成にはいろいろな状況によるものがある．たとえば課題のための作文のような企画された状況設定とか，informalな議論であるとか．

###関係するNLP技術
* automatic essay grading
* detecting meaning errors
* plagiarism detection
* quality assessment

###なぜこのような評価が必要か
* 学生の理解度を本人にフィードバックする
* 学生の到達度を教員にフィードバックする
* インセンティブとして
* 進級判定などへの定式化されたデータとして

単純な多肢式問題による評価と比べて，自由作文のような形式の問題に対する評価は主にコストが問題．そこで自動評価を用いる．

ここではessay gradingを例とする．

###Essay grading
ここでいうEssay:

* 中等教育で構造的な書き方を習うように，義務教育の主要なもの
* 大学入試選抜にも使われる
* ある分野の理解の度合いを判断したり，学生が研究分野について説明したり意見を述べるなどを求められるときのもの

####Most prominent systems
* Intelligent essay assessor 1998
    * 統計ベースで，文中の単語間の関係を要約して...というもの．すべての単語が素性．
* Intellimetric 2001: used undisclosed features
* Project essay grade 1994
* E-rater

####How do we and machines rate essays?
人間が採点: 本質的な部分（中身・文構造・論証・語法・文法・流暢性）  
機械が採点: それらとの近似と可能な関係（採点モデル）

Scoring Modelのつくりかた

* 数百のessayを分析
    * 可能な限り多人数による事前の採点があると良い
* どのような近似をするか（素性をどうするか）
* 統計的モデリングを用いて素性を統合

####Skepticism and Criticism
* 人間のようには採点できないので，結局のところ自動採点が教員の代わりになるわけはなく，支援として使われる．
* 学生のなかには「不誠実な」essayを書くものもいるから，その影響を抑えるためにデータは大量に必要．
* 自動採点のための尺度はessayにとって本当に重要な基準を反映しているかどうか．

####Example of features
Grammar, usage, typos, style, organisation&development, lexical complexity, prompt-specific vocabulary usage

these were used in e-rater

###Writing analysis tools
Correctness  
5つの主要な文法誤り・語法の誤り
>agreement and verb formation errors, wrong word use, missing punctuation, typographical errors

Corpus based approach  
大規模な編集済みテキストのコーパスから単語bigramと形態素を抽出．そして対象となる入力文(essay) のbigramからコーパスで頻度が低いものを間違いとする．

Aspect of style  
なにを校正したいか;  
受動態の使い方・長すぎたり短すぎたりする文・過度に繰り返しの多い単語

Organization&Development   
談話的な要素がessay中に現れているか，欠けているか:  

* 導入
* 問題提起
* 主題
* 補題
* 結論

のような流れがあるか，ということ．

Lexical complexity  
単語に特有の特性，に関係するもの．例えば語彙力に基づくものや単語長の平均に基づくもの．

Prompt-Specific Vocabulary Usage  
直感的には，良いessayは同じような単語選択をしていること．だめなessayは同様にだめな単語選択をしているということ．  
それを反映するためには，同程度の点数のessayと比べるということをする．

* それぞれの得点圏クラスタの訓練セットは素性ベクトルに変換される．
* 機能語は除去される
* ベクトルの成分は単語の頻度で重み付けされる
* コサイン類似度で入力文と得点カテゴリとの類似度を測る

####The future of writing assistance
* 採点基準の一般化
* 様々な評価基準を取り入れる
* 作為的な良くない文を検出する
    * 例えばoff topicなものを取り除くとか．

###Plagiarism: 剽窃検知
剽窃; 学生からのコピー，外部からのコピー，自分の書いたものをコピー  
Internet makes it easier than ever

見つからないように... 単語を入替え・書式を変更・見出しを変えたり・順序を変えたり・イギリス英語にしたり・変数名を変えたり

見つけるために... 学生のレベルから乖離した専門用語・以前の提出物からの飛躍的な成長・前後の段落とのつながりの悪さ・同じような文を提出した学生がいるか・同じようなスペルミスをするか・参考文献の欄にあるか

####How: String matching
最もよく使われる手法．
>The longer n becomes, the more unlikely it is that the same sequence of n tokens (words or characters) will appear in the same order in independently written texts

しきい値の設定が難しい．

####How: Longest common substrings computed between two sentences
2つの文の最長部分一致文字列をみる．

###NLP for Plagiarism Detection
>Areas of NLP that could aid PD, particularly in identifying text which exhibit similarity in semantics, structure or discourse, but differ in lexical overlap and syntax

NLP methods that could be useful:  
morphological analysis,  POS tagging, anaphora resolution, parsing, co-reference resolution, word sense disambiguation, discourse processing

###Short textual question: assessment
Automatic scoring, Automatic diagnosis

###Automatic scoring speech
* Non native speech scoring (Bernstein 1999 + )
* SET-10 (Bernstein) predicts word sequence
* TOEFL practice online speaking test (Zechner et al. 2007)
* Test with Heterogeneous tasks (THT)

####THT
評価の次元: 包括性・精度・明瞭性・一貫性・適切性

評価に用いるもの: 発音・流暢かどうか・文法または語彙の使用・意見の形成・対話コンテキストへの反応

SpeechRater: TOEFLスピーキングのコーパスで訓練

##Reading / writing assistance
###Reading: Readability
Flesch index, Fog index, SMOG grading

難読性についての統計的モデリング:  
Brown & Eskenazi 2004
>Lexical features: word unigrams (Collins-Thompson & Callan, 2005; Heilman et al., 2008)
Grammatical features: frequency of specific grammatical constructions

読解訓練のための文章抽出の応用  
読解力についての問題は広く存在する．（アメリカの公立高校生のわずか29%が十分に読解力があるという評価．．．）  
チャイルドシートの使用法を読めなかったせい不適切な使用法が行われていることがあるという．その使用説明は8割のアメリカ人にとって難しすぎるものらしい．

Readability 評価  

* Read-X(Miltsasaki&Troutt 2008)
* REAP search

####Text Simplification
Readabilityは文章の簡略化で改善できる．
人手による簡略化の特徴:  

* より短い文
* より少なく短い節
* 形容詞・副詞・等位接続詞の減少
* 名詞が代名詞に置き換えられる

自動簡略化:  
統語的または語彙的簡略化

####Vocabulary Assistance for Reading
第二言語学習者や子供のために．    
問題  
>文脈がその単語の意味について判断するに十分な情報をもたない

目標  
>わからない単語について注釈を動的に生成して，文章を増強する

Resource:  
* WordNet
* Wikipedia/Wiktionary: could be useful if it is mined

#####Wikify! (Mihalcea & Csomai, 2007)
Aim:  
文章中の重要な概念・キーワードを対応するWikipediaのページとリンクさせる．

Keyword extraction:  
tf-idf, カイ二乗検定, Keyphrasenessでランキング

Word sense disambiguation  
Lesk Algorithm,  分類器によるもの

###Spelling error detection and correction
Aim:  
スペルミスの特定と訂正

Types of spelling errors:  

* ただの綴り間違い
* 区切り間違い
* 実世界で起こりがちな音の混同（there-theirのようなもの）

####Research Overview
単純な綴り間違い  
>パターンマッチングの効率化など

文脈依存の単語訂正
>80’から. 統計的言語モデル

Textbook:  
>Jurafsky&Martin 2008, Manning, Raghavan and Schutze 2008

####Non-Word Error detection
ngram手法か辞書引き

####Isolated word error
編集距離でおこなうことが多い.  

>人間ですら100%の訂正はできない.

本質的な訂正候補の曖昧性があるから.

####Context-dependent
Aim:  
実世界で起こりうる，辞書引きでは対応できないタイプの誤りを訂正する

25-40%の誤りが正当な英語の単語であることから，文脈を利用して訂正する必要がある．言語モデルに基づく．

####Spelling Correction for Foreign Language Learners (Heift&Rimrott 2007)
背景:  
>80% of the misspellings produced by non native writers of German are due to insufficient command of Foreign Language

このような場合は既存のスペルチェッカーでは難しい．L2の誤りやすさを考慮する必要がある．

また，訂正候補の提示などによるフィードバックは大事．

###Grammar Checking
Tasks:   

* Grammatical error detection: identify sentences which are grammatically ill formed
* Grammatical error correction: correct grammatically ill formed

Methods:  

* Rule based
* Syntax based
* **Statistics based**: usually focus on a specific POS

####Rule based
Usually based on POS information.  
Examples: Language tool (Naber 2003), GRANSKA(Eeg-Olofsson&Knutsson 2003)

####Syntax based
Template matching on Parse trees (Lee&Seneff 2008)  

####Statistics based
POS bigrams(Atwell 1987)  
POS tags and function words ngrams (Chodorow& Leacock 2000)

Machine Learning:  
Maximum Entropy model (Chodorow et al 2007)  
Machine learning on automatically labelled sequential patterns (Sun 2007)

###The Tip of the Tongue Problem
言葉が喉まで出かかっているのに出てこない，みたいな問題．  
普通の辞書引きでは対応できない？？

(Ferret&Zock 2006) ではmental lexiconを用いている．単語対その概念という関係をもつネットワーク構造．

##Tutoring Systems
Goal: the ability to engage learners in rich natural language  
人間による教室外での個人指導はもちろん，それを機械に置き換えた場合でも学習の効果は向上している(Corbett 1999)

特徴:  
システムは問題文と設問を提示  
学習者は自然言語で解答  
複数の長い発話・文が解答になりうる

####Corpus based studies
*  Speech acts in tutorial dialogue (Marineau et al. 2000)
* Dialogue acts' correlation with learning (Forbes-Riley et al.
2005, Core et al. 2003, Rosé et al. 2003, Katz et al. 2003)
* Student uncertainty in dialogue (Liscombe et al. 2005, Forbes-Riley and Litman 2005)
* Comparing text-based and spoken dialogue (Litman et al. 2006)

###AutoTutor
計算機科学基礎の受講生のための，支援システム．  
訓練された|未熟な チューターの対話を模擬することも目的．

Architecture (important points):  
* Speech act classifier
* Latent semantic analysis

How to engage the student in conversation?:  
* Open ended question.
* AutoTutor decides what expectation to handle next and

##CALL2.0 is realised by advanced web
Huge amount of data enabled new era of e-learning

* Community rule based grammar checking [link](http://community.languagetool.org)
* Social QA sites/forums

###Weimer&Gurevych 2007, Predicting the perceived quality of web forum posts
>Aimed at developing a system to automatically assess the perceived quality of forum posts

Related to Essay scoring, automatically assessing review helpfulness

required to be fit with the standards of user community , independent of metadata-based feature, to be able to adopt to different domains

##Example: Semantic Information Retrieval for Electronic Career Guidance
どのような専門的職種に就きたいかという作文にをクエリとして，それらの職業の説明文章から情報抽出することで，技能のランキングリストを提示する．

このときにSemantic情報を用いた文章間類似度を用いたりする．

##Some thoughts on eLearning2.0
eNLPが役立つこと: 知識獲得のボトルネックを解消・新しいeLearningの形

NLPにとってはよい遊び場？: eLearning2.0の談話的なところはほとんど手を付けられていないから！

BioNLP??

###eNLPを流行らすには...
* 国際的コミュニティの確立
* ACL関連の会議
* 関連チュートリアル
* タグ付済みコーパス・ツールなどの資源

###このチュートリアルで触れていないこと（たくさん！）
* CALL
* Intelligent tutoring system
* Information search for eLearning
* Educational Blogging （これはあまり聞いたことがない）
* Annotations and social tagging
* Analysing collaborative learning processes automatically
* Learner's corpora and resources
* eLearning standards, e.g. SCORM


