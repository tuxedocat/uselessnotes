# stdlib: logging
## Enabling/Disabling logging
```python  
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',
                    level=logging.DEBUG)
```
If disabling logging msg. is needed,
```py
logging.disable('debug')
```
makes logging module not to output messages below `debug` level.


# stdlib: collections
## defaultdict
```py
import collections
d = collections.defaultdict(list) # dictionary which values are empty list
```

## Counter
> fast, easy to use container to count some objects up

```py
import collections
pets = ['cat', 'cat', 'dog', 'cat', 'mouse', 'dog', 'cat']
c = collections.Counter(pets)

>>> c
    
    Counter({'cat' : 3, 'dog' : 2, 'moust' : 1})

```

This also supports some operation, like intersection, adding...

### Updating the Counter

```py
c_sum = collections.Counter()
c_tmp = collections.Counter(sequence)
c_sum = c_sum + c_tmp
```

# stdlib: datetime, time, timeit, dateutil, etc.
## Adding date to log file's name
```py
>>> from datetime import datetime
>>> datetime.now().strftime('mylogfile_%H_%M_%d_%m_%Y.log')
'mylogfile_08_48_04_02_2012.log'
```
