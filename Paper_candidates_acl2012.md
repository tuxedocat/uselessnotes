# ACL paper reading list
* Verb Classification using Distributional Similarity in Syntactic and Semantic Structures 
 Danilo Croce, Alessandro Moschitti, Roberto Basili and Martha Palmer 

* Word Sense Disambiguation Improves Information Retrieval 
   Zhi Zhong and Hwee Tou Ng

* Cross-Domain Co-Extraction of Sentiment and Topic Lexicons 
 Fangtao Li, Sinno Jialin Pan, Ou Jin, Qiang Yang and Xiaoyan Zhu 
 
* Learning Syntactic Verb Frames using Graphical Models 
   Thomas Lippincott, Anna Korhonen and Diarmuid O Seaghdha

* (not appeared)Learning Verb Inference Rules from Linguistically-Motivated Evidence 
 Hila Weisman, Jonathan Berant, Idan Szpektor and Ido Dagan

* Applying Collocation Segmentation to the ACL Anthology Reference Corpus 
 Vidas Daudaravicius

* Collocation Polarity Disambiguation Using Web-based Pseudo Contexts 
 Yanyan Zhao, Bing Qin and Ting Liu

* Assessment of ESL Learners' Syntactic Competence Based on Similarity Measures
 Su-Youn Yoon and Suma Bhat

* State-of-the-Art Kernels for Natural Language Processing
 Alessandro Moschitti   Lotus 1

* Topic Models, Latent Space Models, Sparse Coding, and All That: A systematic understanding of probabilistic semantic extraction in large corpus
  Eric Xing

* Automated Essay Scoring Based on Finite State Transducer: towards ASR Transcription of Oral English Speech 
 Xingyuan Peng, Dengfeng Ke and Bo Xu

* Discriminative Strategies to Integrate Multiword Expression Recognition and Parsing 
 Matthieu Constant, Anthony Sigogne and Patrick Watrin

* A Discriminative Model for Query Spelling Correction with Latent Structural SVM 
 Huizhong Duan, Yanen Li, ChengXiang Zhai and Dan Roth 

* Characterizing Stylistic Elements in Syntactic Structure 
   Song Feng, Ritwik Banerjee and Yejin Choii

* Lyrics, Music, and Emotions
 Rada Mihalcea and Carlo Strapparava

* A Beam-Search Decoder for Grammatical Error Correction
 Daniel Dahlmeier and Hwee Tou Ng

* Native Language Detection with Tree Substitution Grammars 
 Benjamin Swanson and Eugene Charniak

* Syntactic Stylometry for Deception Detection 
 Song Feng, Ritwik Banerjee and Yejin Choi

* Crosslingual Induction of Semantic Roles 
 Ivan Titov and Alexandre Klementiev


##Short papers
* Movie-DiC: a Movie Dialogue Corpus for Research and Development
 Rafael E. Banchs

* Grammar Error Correction Using Pseudo-Error Sentences and Domain Adaptation
 Kenji Imamura, Kuniko Saito, Kugatsu Sadamitsu and Hitoshi Nishikawa

* A Meta Learning Approach to Grammatical Error Correction
 Hongsuck Seo, Jonghoon Lee, Seokhwan Kim, Kyusong Lee, Sechun Kang and Gary Geunbae Lee


## System Demo

* FLOW: A First-Language-Oriented Writing Assistant System
 MeiHua Chen, ShihTing Huang, HungTing Hsieh, TingHui Kao and Jason S. Chang 

* A Graphical Interface for MT Evaluation and Error Analysis
 Meritxell Gonzàlez, Jesús Giménez and Lluís Màrquez 

* LetsMT!: Cloud-Based Platform for Do-It-Yourself Machine Translation
  Andrejs Vasiļjevs, Raivis Skadiļš and Jörg Tiedemann
