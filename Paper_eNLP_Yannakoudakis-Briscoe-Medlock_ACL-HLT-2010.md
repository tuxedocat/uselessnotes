latex input:        mmd-article-header  
Title:                  Helen Yannakoudakis, Ted Briscoe, Ben Medlock, "A New Dataset and Method for Automatically Grading ESOL Texts", ACL HLT 2010
Date:                 today  
Author:  yu-s
Base Header Level:  2    
LaTeX Mode:         memoir   
latex input:        mmd-article-begin-doc  
latex footer:       mmd-memoir-footer  

>Helen Yannakoudakis, Ted Briscoe, Ben Medlock, "A New Dataset and Method for Automatically Grading ESOL Texts", ACL HLT 2010

###Abstract
本論文では教師あり機械学習の技法がESOLの試験作文の採点・評価に利用できるかを示す．
特に，本論文ではランキング学習を作文間のGrade関係を明示的にモデル化するために用いた．
多数の素性を抽出する．それらが全体にどれぐらい寄与するかを測るために部分的なテストを行った．
回帰と提案手法との比較の結果，提案手法のほうがずっと良かった．
公開されているデータセットに対する実験結果は，人手による採点というタスクの上限に迫る性能を達成できることがわかる．
最終的にはoutlier(異常な)文を用いてモデルの有効性をテストした．
また，モデルの出すスコアが人手で付けられたスコアと乖離する場合を特定した．

###Introduction
自由作文に対する自動評価のタスクは，writing能力の質の自動解析・評価に重きをおいている．
自動評価システムはテキスト的素性を利用している．全体の質を測ることと，文章を採点するために．
最初期のシステムはとても表層的な素性を用いた．たとえば単語や文の長さである．それらを文章理解の手がかりとする．  
より最近のシステムはもっと洗練された自動テキスト処理技法を，文法性，文章の一貫性，予見できる誤りなどを測るために用いている．

自動評価システムを用いると数々の利点がある．特に大規模な評価時における採点の労力削減のようなことである．
さらに，採点基準が同じであることを保証するから，複数人による採点の際に問題になる不一致を減らすことになる．
ときに，書き手の能力に応じたフィードバックを含んだ実装では，self-assessment(自己評価)やself-tutoring(自己指導)を促す．

暗黙的にだったり明示的にだったりするが，これまでの研究は自動評価を教師あり文章分類タスクとして扱うことがほとんどであり，
この場合学習文はgrade(習熟度?)でラベル付されており，テスト文を分類器の出力に適用された回帰ステップから同じ採点基準にフィッティングする．
また，ほかの手法もある．

* Attali and Burstein 2006: 多様な手法でベクトル表現された文章のCosine類似度
* Landauer et al. 2003: Latent Semantic Analysis (LSA)による次元削減との組み合わせ
* Rudner and Liang 2002: 機械学習(生成モデル)
* Attali and Burstein 2006: ドメイン限定素性抽出
* Lonsdale and Strong-Krause 2003: 改良された構文解析器

####自動採点システムについて
Williamson 2009では12種の自動採点システムについて述べている．その一例．

* e-Rater (Attali and Burstein 2006)
* Intelligent Essay Assessor (IEA, Landauer et al. 2003)
* IntelliMetric (Elliot 2003, Runder et al. 2006)
* Project Essay Grade (PEG, Page 2003)

中には試験作文の採点に用いられているものもある．
このようなシステムに対する性能評価は多く行われているが，
一方で，システム間の性能比較や学習・テストのための公開データセットは存在しない．
システムの利用数は増えるであろうから，独立で標準化された評価方法は重要になる．
(*このグループの用いたデータセット: CLC．この論文はCLCに対する提案システムの結果・評価を含む*)

####問題の取り扱い
**本論文では自動評価を機械学習の問題―教師あり識別問題―として，特にrank preference problem(順位選好問題)として扱う．**

文章分類においては識別分類の手法はときに非識別的な手法を上回る．(Joachims 1998)
加えてrank preference手法(Joachims 2002)は文章の質についての最適ランキングモデルを明示的に学習することを可能にする．
学習後に分類器のスコアをGradeのスケールにフィッティングするより，むしろランキングを直接学習するのは，このタスクにはより一般的な手法であるうえに，学習データ中のラベル情報を直接，効率的に利用できることになる．

LSA(Landauer and Foltz 1998)のような手法では，Writing能力に加え，与えられた設問に対する回答の意味的な類似度を測る．
しかし，人手により採点されたCLCが自由作文の解答を引き出すような，学習者のテキストからつくられたものの，
採点基準は主として様々な言語的な構造の正しい使い方に基づいている．
このため，ESOL文章の評価のためには，言語能力を直接測るような手法が向いていると考えている．
また，このような手法は新しい問題やタスクのために再学習しなくてよいかもしれないという利点があるだろう．

著者らが知る限り，自動評価(AA)タスクへのrank preference modelの初めての応用となるだろう．  
実験:

* 比較的少量のデータで訓練されたrank preference SVMでの実験
* 一般のテキスト処理ツールで自動的に得られた特徴の同定
* regressionSVMとの比較
* outlier(異常な)文に対する最もよいモデルの頑健性について

一貫した・比較可能な・再現可能な結果を新しいデータセットと公開済みのツールやデータに対して報告する．
一方で，AAのための新しい素性を実験的に与えることもおこなう．(Briscoe 2010の発展)

適切な素性を用いた識別モデルの手法は同コーパスに対する人手による性能の上限に迫る性能を達成したということを実験的に示す．

###Cambridge Learner Corpus
(paragraph 1: omitted)

* First Certificate in English (FCE)テストの学習者作文を抽出
    * Upper-intermediate level (中上級者・TOEFL IBT 70-90, TOEIC 650-800らしい)
* XML，設問・学習者のgrade・母語・年齢のメタデータ付き
* FCE: 以下のうち2つの設問(between 200-400words)
    * letter, report, article, composition, short story
* 各設問に対するスコアは1-40の40段階
    * 採点者間の不一致を解消するためにFischer and Molenarr 1995のRASCHモデルによるフィッティングがされている
* 各タスクには全体に対するスコアも付いている

**各学習者作文はNicholls 2003による約80種の言語学的誤りのアノテーションが人手で付与されている．**
たとえば，
>In the morning, you are \<NS type = "TV"> waken|woken \</NS> up by a singing puppy.

この文においては，"TV"は動詞の時制誤りを示す．"waken"が"woken"に訂正されることを示す．

CLC-FCE-released:

* 2000・2001年の1141+97作文(それぞれ異なる学習者による)
* 年齢分布は2つの山をもち，16-20と26-30がピークである．

設問はデータセットに含まれているが，**その情報は本手法では全く使われていない**．
設問やトピック限定の学習を必要としない手法を開発するためである．

設問の例:
>Your teacher has asked you to write a story for the school’s English language magazine. The story must begin with the following words: “Unfortunately, Pat wasn’t very good at keeping secrets”

###Approach
* AAタスクをrank preference learningとして扱う．
* SVMはSVM lightを使用．
* 多様な言語学的素性は自動で抽出され，それらがどれくらい性能に寄与したか評価される．

####Rank preference model
Smola 1996のEpsilon-insensitive loss function (イプシロン非感受性損失関数)を用いることで
SVMで回帰をおこなうことができる．
回帰におけるSVMは学習データに基づいた，実数値を出力する関数を推定する．
(分類・回帰どちらもマージン最大化により計算される超平面をもつ)

Rank preference SVMでは，ランキング関数を学習することが目的となる．
(各データ点に対してスコアを出力し，そのスコアからデータ全体の順序付けがされるような関数)
ここでRを学習事例**xn**(vector)とそのランクr(scalar)からなる集合とする． 

(式1)  
R: ri  ＜ rj のときに **xi** succ.(順序関係"＞") **xj** が成り立つような集合．
ただし(1 ≦ i, j ≦ n , i ≠ j)


Rank preference modelはこの事例ベクトル・ランクの集合から直接学習するのではなく，
ペアワイズな差分ベクトルの集合から学習する．
線形ランキングモデルの目的は，正しくランク付けされたペアの数を最大化する重みベクトル
wを求めることである．

(式2)  
これは以下の最適化問題(最小化問題)を解くのと同値である．  
(式3)

(式4・5)という拘束条件のもとで．

Cによって学習誤りとマージンのトレードオフが可能で，ξは誤分類の度合いを測る変数である．

上の最適化問題はペアワイズ差分ベクトルについての分類問題と等価である．
この場合般化は，近いランクが与えられたデータ間の差を最大にすることで与えられる．

ランク学習をAAタスクに適用する第一の利点は，
作文間のgradeの関係を明示的にモデル化するということであり，
スコアリングのスキームに分類器の出力をさらに回帰する必要がないということである．
本論文で述べられる結果は線形識別関数によるものである．

####Feature set
Briscoe et al. 2006によるRobust Accurate Statistical Parsing (RASP)のtokenisationa・sentence boundary detectionモジュールによって，学習・テストデータを構文解析した．
本タスクに適切な素性候補を増やすために以上のことを行った．
実験で用いた素性は主に，AAタスクに特徴的であるべき語彙的・文法的素性で与えられる．

#####Feature set

1. Lexical ngrams
    * 単語unigram
    * 単語bigram
    * 小文字化・活用形の状態で用いられている．
1. POS ngram
    * POS unigram
    * POS bigram
    * POS trigram
        * RASP TaggerのCLAWS tagsetを用いて抽出された．
        * ある単語が複数のPOS Tagをもつ可能性がある場合，
        最尤の候補の確率が0.9以下かつ残りn個の候補が0.02以上のときには，
        RASP Parserのオプションを用いて解析した．
1. Syntactic features
    * 句構造規則(PS)
        * `V1/modal bse/+-, ‘a VP consisting of a modal auxiliary head followed by an (optional) adverbial phrase, followed by a VP headed by a verb with base inflection’`  
        のような規則名が半自動的に生成される．
        * 規則名は稀な構造を明示的に表す．たとえば，   
        `S/pp-ap s-r, ‘a S with preposed PP with adjectival complement, e.g. for better or worse, he left’`  
        * 構文規則外と思われるものや，断片的で不完全なシーケンスもつぎのように表される．  
        `T/txt-frag, ‘a text unit consisting of 2 or more subanalyses that cannot be combined using any rule in the grammar’`
        * この素性によって長距離に及ぶ文法的構造や誤りを
        暗黙的にとらえることができるだろう．
    * 文法的関係の距離(GR)
        * 多様な文法的複雑さに関する尺度がParsingから得られ，
        それらが精度に与える影響が調査された．
        * 実験ではRASPのGrammaticalRelation出力による，
        GR内の主辞と従属との最長距離の合計を表す複雑性尺度を用いた．
        * その尺度は，各文に対する上位10個のParsing結果からの
        各GRグラフについて計算される．
        * 各文に対するそれらの距離の平均と中央値を抽出し，
        各作文における最大値を用いた．
        * 直感的には，この素性は書き手の文法的洗練性に関する情報をとらえることになる．
        * しかし，例えば句読点の付け方が悪くて文境界が特定されない場合に，混乱を招くかもしれない．

1. Others
    * 作文長
    * 誤り率
        * CLCには人手による言語学的誤りの情報が付いている．
        * 本研究では人手によらずに誤り率を求めようとするものの，
        評価時の上限として人手による情報を用いた．
        * true CLC error-rate: CLCの誤りタグから求められ，
        自動誤り検出の評価時における性能上限として用いられる．
        * error-rate推定のために，trigram言語モデルをukWaC LMを用いて作成する．
            * 20億トークン以上の英語コーパス
        * CLCLMのサブセットから抽出されたtrigramでLMを拡張．
            * CLCはESLの文を含むので，
            誤りtrigramが含まれないように高ランクの作文から頻度上位のものだけを用いた．
        * テストデータ中の単語trigramは，LM中に存在しない場合誤りとしてカウントされる．
        * Bloom Filterで効率的にエンコードされたLMからpresence/absence効率を求める．

Lexical ngrams, POS ngrams素性はtf-idfによって重み付けと，
L2ノルムによる正規化がされる．
Syntactic featuresは頻度によって重み付けされる．
Syntactic, Others素性は重みがLexical，POS素性と同じオーダーの大きさになるようにスケーリングされる．

作文長は単語数に基づいており，作文長が他の素性に与える影響を調整するために加えられる．
**頻度4以下の素性はモデルから取り除かれる．**

###Evaluation
AAシステムの評価には以下の2つの相関尺度を用いる．

* Pearson's product-moment correlation coefficient  
(ピアソンの積率相関係数)
    * 2変数の直線的関係の指標
    * データの分布に敏感であるため，外れ値によって値が誤ってしまうことがある．
* Spearman's rank correlation coefficient  
(スピアマンの順位相関係数)
    * ノンパラメトリック(ふたつの変数の分布についての仮定が何もない．変数間の関係が任意の単調関数でどの程度表現できるかを評価する．順位さえ明らかであればよい．)
    で頑健な関係性の尺度で，値の順位にのみ感応する．
    * データには同順位のものもあるため，
    本論文では順位に対するPearsonの相関係数を計算した．

####Correlation between the CLC scores and the AA system predicted values (Table. 1)
CLCのデータに付いている採点データとの相関を測る．素性を追加して行ってその寄与を見る．
Error-rate素性は，その計算に用いたLMで分けられている．

* 基本的にどの素性も良い方向に寄与している．
* true-CLC-ERとestimated-ERはPearson相関係数で0.611だった
    * これはLMの改良の余地があることを示唆する
    * 結果からukWaC+CLC LMをError rate計算に用いた

####Ablation tests showing the correlation between the CLC and the AA system (Table.2)
切除テスト: ある素性だけ取り除いた場合の評価．取り除いた方が"none"より良かったらその素性は悪い影響を与えている．

* Phrase-Structure-Rule素性・Error-rate(ukWaC+CLC LM)素性は大いに寄与している
* POS-ngrams素性は寄与している
* word-ngrams素性はPearson相関係数には寄与している
* complexity・script length素性はあまり寄与していない

####one-tailed t-test (a=0.05)
* POS・PS・complexity・error-rate素性はSpearman順位相関係数に優位に寄与している
* PSはPearson相関係数にも寄与している

####Comparison between regression and rank pref- erence model (Table.3)
回帰SVMとの比較．AAタスクの場合Rank-preference-SVMの方が適している．

* Rank-preference-SVMは，Pearson:+0.044，Spearman: 0.067改善できている

####Comparison between the CLC Score and four human examiners
タスクの性能上限を求めるためにCLCのデータの点数と採点者のものを比較．

* Pearson, Spearman = 0.792，0.796 が上限

####Pearson's/Spearman's correlation of the AA system predicted values with the CLC and the examiners’ scores (Table.4,5)
採点者E1-E4とRASCH調整済みのCLC,およびこのAAシステム間の各相関係数の表.
この結果から，採点者の採点とAAシステムによる採点は十分比較可能であること，
タスクの上限に近い性能を発揮していることがわかる．

* Table4: AAシステムとE4は比べられない
* Table5: AAシステムとE1, E4は比べられない
* システムは人間と違い，文脈の一致性や設問との関係性を採点していないから

###Validity test
システムの特性を利用して点数を稼ごうとする書き手に対する頑健性を調査する．
AAシステムの頑健性に言及しているものは少ないものの，
Powers et al. 2002では初期のe-Rater (Burnstein 1998)について述べており，
そのシステムが比較的ドメイン限定の言語学的素性を用いていることがわかれば，
システムを欺いて高いスコアを付けさせるのは，低いスコアを付けさせるよりは楽なことがわかった．

提案システムがどのような素性に対する文章生成技法を脅威とするのか調査することが目的．

方法:  
6つの高得点のFCE試験作文(Overlapはなし)から以下のように生成．
計30の「異常な」文ができる．

* 順序のランダム化
    * 単語uni-gram
    * 単語bi-gram
    * 単語tri-gram
    * 文章中の文
* 同一文中の同一POSタグを持つ単語を入替える

####Correlation between the predicted values and the examiner’s scores on ‘outlier’ texts (Table.6)
異常な文に対する，システムの採点と採点者の採点とのCorrelation

* uni, bi-gramではそれほど影響はなかった
* trigram では0.8程度と低くなるため，Nが大きいほど悪くなる傾向にある
* 同POS単語の入替えでは上の項目より相関係数は低くなった
* システムは採点者に比べて高めの点数を出す傾向がある．
* 文単位では0.08・0.163と非常に悪い
    * システムは文脈の一貫性や矛盾を評価しないため，作文全体を評価する採点者が極めて低い点数を付けたものを，システムは高く採点した．
* このような高度な技法は既に習熟した学習者がおこなうが，次期バージョンではこれらに対応する文脈情報を用いる．

####余談(?)
悪意をもった学習者の作文はイギリスメディアの話題にのぼった．
例えばAAシステムをチャーチルの対ドイツ戦演説に対して適用したら，
過剰な繰り返しが含まれているために，低い点数をつけてしまった．
我々が提案するモデルはチャーチルの演説にもちゃんと高いスコアをつけたが，
最もよい点数というわけではない．
ジャーナリストたちがふさわしいと感じているような点数は...

###Previous work
See, Pez-Marin et al. (2009), Williamson (2009), Dikli (2006) and Valenti et al. (2003) for more detailed discussion.

* PEG (Page 2003)
    * Linear regression using mostly shallow textual features.
    * 文長とか単語数のような簡単な素性なので，間単にシステムを騙せてしまうだろう．

* e-Rater (Atalli and Burnstein 2006)
    * 文脈・設問との関連性も扱う
    * ベクトル化された文章間のCosine類似度による
    * タスク依存なので設問に合わせた再学習が必要

* IEA (Landauer et al. 2003)
    * LSAによる文章間類似度．
    * 特異値分解を用いて次元削減
    * 学習データと最も類似した作文を求めてスコアを算出
    * 全体の点数は様々な素性から回帰を用いて算出
    * 設問や採点基準などの変更には再学習が必要

* Lonsdale and Strong-Krause 2003
    * Link Grammer parserを用いた
    * 全体の点数は文ごとの点数(5段階)の平均
    * 文の採点はParserのコストベクトルによる．(これで文法的複雑性がだいたい測れる)
    * 本論文で提案する手法でいうところの文法的複雑性の素性に似ている．

* BETSY (Runder and Liang 2002)
    * 多項orベルヌーイ分布のNaive Bayesによる
    * 素性は単語1,2-gram，文長，動詞の数，noun-verbのペアなど．
    * 分類されるクラスはGrade(A-F)であったり合否であったりする
    * 分類器ベースの自動採点の有効性を示したが，素性が少し簡単であることやGradeごとに分類器を学習しているので効率が悪い．

* Chen et al. 2010
    * 同じトピックの作文に対する教師なし学習を用いた手法
    * 投票アルゴリズムを用いる
    * 文章はGradeによってクラスタリングされ，初期Zスコアが割り当てられる．
    * Iterationを繰り返して学習
    * 学習データ中のGradeの分布が最終的なZスコアに当てはめるときに使われるので弱教師あり学習と言ったほうがよいかもしれない．
    * BoW表現なので簡単に騙すことができてしまう．

###Conclusions and future work
* 様々なAAシステムが提案されてきたにも関わらずシステム間の評価は共通データセットの不在のために行われて来なかった．
    * 本研究でリリースしたESOL作文の公開テストデータセットが広く使われることを願う．
* ランキング学習モデルを用いた提案手法は一般的な素性を用いており，人手によるタスクの性能上限に迫る結果を達成した．
    * 切除テストにより各素性の寄与が示された．
    * 回帰モデルとの比較によってランキング学習モデルの方が優位であることを示した．
    * どのようなOutlier(異常な)データによりシステムが欺かれるかを示した．

Future work:

* よりよい誤り検出手法を用いて更なる実験を行う．
    * Error-rate素性が特に寄与していたから．
* Briscoe 2010では再学習の要らないOff-topic検出手法が提案されており，本AAシステムを組み合わせるつもりである．
* n-gramで捉えられる範囲外の意味的な相関性や文脈の一貫性の有効性はOutlierデータの実験でわかったので，それを用いる．
* AAシステムの素性集合と干渉しない指標を加えることで性能が著しく向上するという報告もある．
