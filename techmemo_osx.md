# File saving dialogue
## Change default saving location from iCloud
```sh
defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false
```

To revert...

```sh
defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool true
```

# Disk utility
## Show the list of disks
```sh
diskutil list
```
## Mount/Unmount all partitions on a disk
```sh
diskutil mountDisk <diskid>
diskutil unmountDisk <diskid>
```
## Mount/Unmount
```sh
diskutil mount <diskid>
diskutil unmount <diskid>
```

# Time Machine
## Do xx from the commandline
>Note: Those are available only OS X 10.7 or later

### Enabling/Disabling
```sh
sudo tmutil enable
sudo tmutil disable
```

### Perform backup-now from the commandline
```sh
tmutil startbackup
tmutil stopbackup
```

### Changing the target disk
```sh
sudo tmutil setdestination /Volumes/newtargetdisk
```