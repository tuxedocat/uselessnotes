20120721 Paper reading

> Michael Gamon. 2010. Using mostly native data to correct errors in learners' writing: A meta-classifier approach

Summary
================
* Meta classifier improves prepotition and article error correction
* Using only 7gram language model is still achieve the state of the art performance
* For prepotition errors, adding at least 10% of training data to LM improves performance significantly
* For article errors, adding 1% of training data to LM improves performance significantly


Motivation
================
Meta Classifierを用いて冠詞・前置詞誤りを訂正する．

Previous works
----------------
* 分類器ベースのものでは，大抵MaxEntClassifierが用いられる
* 冠詞・前置詞に注力する研究が多い理由
    * Closedなものだから(前置詞でも数百だけ，高頻度のものに絞れば数十種)
    * 冠詞と前置詞は最も多い誤りであるから
    * 冠詞は，英語がもつ限定性をそれほどもたないL1の学習者にとっては，最も難しい
    * 前置詞はどのようなL1の学習者にとっても難しい


Approach
----------------
* 2つのdata driven modelを使う
    * primary models: 誤り特化型モデルと巨大な言語モデルのこと
    * meta classifier: 言語モデルのスコアとclassifierの確率値を受け取る
* 利点
    * primary modelは巨大なネイティブコーパスからの言語モデルを利用できる
    * meta classifierは少量の誤りタグ付きコーパスから学習できる
    * well formedなネイティブコーパスと学習者コーパスの違いをmeta classifierによって"fine tune"するということ
* 主な目的
    * Meta classifierがどれくらい有効か
    * どの程度のタグ付きデータを追加すればよいか


Details
================

Primary models
----------------
* Maximum entropy classifier
    * with -6,6 token window 
    * lexical, pos, 'custom features'
* presence classifier (前置詞・冠詞の有無を判別
    * すべてのNPの境界がチェックポイント
* choice classifier (候補の選択)
    * 前置詞: 12の高頻度のものに限る
* Training
    * 2.5M Sents (Reuters, Encarta, UN proceedings, Europarl, original web data)
* 前置詞・冠詞それぞれの挿入・削除・置換(6種類)の組み合わせの中から上位3つを利用する
* 言語モデル
    * English Gigaword， 7-gram, Absolute Discounting smoothing
* choice classifierの出力を言語モデルで評価する

Meta classifier
----------------
Decision treeを用いる(WinMine Toolkit)
(非線形カーネルのSVMが代替になりうるが試していない)

* 素性(前置詞で63個，冠詞で36個の次元)
    * Ratio and delta of Log LM score (元の語と訂正候補についてのLM上でのスコア比・差分)
    * 上のもののエントロピー
    * さらにそれらと分類器出力(有無と選択確率)の積 
    * 操作の種類(3つの素性)
    * 有無(1つ)
    * 選択確率:前置詞12+1(空欄)と冠詞(2つ)
    * 元の語
    * 訂正候補
* 学習事例
    * ラベル: Binary(正解・不正解)


Data (meta-classifier)
----------------
CLC, training:70%, test:20%, development:10%

* それぞれの誤りのための学習事例作成
    * 各種クリーニング
        * 学習者の入力は予めスペルチェッカーにかけられると想定して，スペル誤りは訂正しておく
        * その他の誤りは，より現実的な設定のためにそのままにしておく(ただし前置詞・冠詞誤りと隣接したり入れ子式になっているものは除外する)

Evaluation
----------------
* Precision・Recallはmeta classifierの出力の閾値
* 言語モデル単体でのP・Rも調べた
    * deltaを閾値にした; 差が大きいと事例は減るが信頼度が高まるという結果

* 多くの関連研究での評価と異なるかもしれない
    * CLCは様々なレベルの学習者文を含むし，とても巨大
* 提案手法で提示された訂正候補を人手でチェック



Results
================
* 言語モデルだけでもState of the artの性能であった
* Meta classifierを用いると全域で改善が見られた
* 前置詞に関しては，LM+学習データ1%ではあまり変わらない(10%から向上)
* 冠詞では1%の学習データであっても大きな性能向上が確認できた


Error Analysis
================
* 高頻度の低次Ngramのスコアが支配的だった場合があった(言語モデルが間違うとき)
    * "by newspaper"のようなものが多いと元のものと訂正候補でのスコアの差が大きくなりすぎる
    * Error specific classifierよりも言語モデルの方が精度がよいので，Meta classifierは言語モデルの方を信用してしまう

* 言語モデルは正解なのに分類器が間違えた場合


Future work
================
* 異なる様々なデータセットで学習した分類器，または学習アルゴリズム
* SkipモデルやPOSのNgramなどの言語モデルを用いる










