latex input:        mmd-article-header  
Title:                  eNLP
Date:                 20111101  
Base Header Level:  2    
LaTeX Mode:         memoir   
latex input:        mmd-article-begin-doc  
latex footer:       mmd-memoir-footer  

##論文紹介
Rozovskaya, Roth_Generating Confusion Sets for Context-Sensitive Error Correction_Proceedings of EMNLP 2010

Confusion set : 何が何に間違えやすいか，というのを記したもの．

この論文は学習者の母国語に基づいたCSを作っておいて，それをどうやって活用するか，ということ．．．

###Abstract
>We focus on the task of correcting errors in preposition usage made by non-native English speakers, using discriminative classifiers. 

通常の場合だと全ての格助詞候補を提示して．．．としているが，本研究ではL2学習者が間違いやすいもの candidates set を用いることで候補を絞る．
また，アノテーション済みコーパスを用いて尤度を出して，それを利用することでより有効であることがわかった．

###Introduction
* これはもともとスペル訂正の分野で使われていた手法．
* 最近ではこれを文法誤りに使ってみようということになってきた．
* 冠詞誤りの場合，候補はa an the null の4つ
* しかし前置詞の場合候補は9〜34である
* 実際のところそれらの間違いの確率は一律ではなく，学習者のL1に応じた傾向がある
* L1 dependent candidates set をTrainingとTestで役立てることを目的とする．

Baseline

* 誤りやすい10個の前置詞に限定
* 多値分類器（1 vs rest）
* ネイティブが書いたものを学習データとするもの

Proposed  

* 学習時のL1dependent set の使い方はふたつ
    * >First, we train a separate classifier for each preposition pi on the prepositions that belong to L1- dependent candidate set of pi. In this setting, the negative examples for pi are those that belong to L1- dependent candidate set of pi
    * 人工的なエラーをつけたもの
* テスト時
    * >We also propose a method of enforcing L1- dependent candidate sets in testing, through the use of a confidence threshold. We consider two ways of applying a threshold: (1) the standard way, when a correction is proposed only if the classifier’s con- fidence is sufficiently high and (2) L1-dependent threshold, when a correction is proposed only if it belongs to L1-dependent candidate set.
    
###Related work
重要なところ

>Each occurrence of a candidate word in text is represented as a vector of features. **A classifier is trained on a large corpus of error-free text. **Given text to correct, for each word in text that belongs to the confusion set the classifier is used to predict the most likely candidate in the confusion set given the word’s context.

過去の研究

>In the same spirit, models for correcting ESL er- rors are generally trained on well-formed native text. Han et al. (2006) train a maximum entropy model to correct article mistakes. Chodorow et. al (2007), Tetreault and Chodorow (2008), and De Felice and Pulman (2008) train a maximum entropy model and De Felice and Pulman (2007) train a voted percep- tron algorithm to correct preposition errors. Gamon et al. (2008) train a decision tree model and a lan- guage model to correct errors in article and preposi- tion usage. Bergsma et al. (2009) propose a Näive Bayes algorithm with web-scale N-grams as fea- tures, for preposition selection and context-sensitive spelling correction.

Error tagged data…

>Rozovskaya and Roth (2010b) show that by intro- ducing into native training data artificial article er- rors it is possible to improve the performance of the article correction system, when compared to a clas- sifier trained on native data. In contrast to Gamon (2010) and Han et al. (2010) that use annotated data for training, the system is trained on native data, but the native data are transformed to be more like L1 data through artificial article errors that mimic the error rates and error patterns of non-native writers. This method is cheaper, since obtaining error statis- tics requires much less annotated data than training. Moreover, the training data size is not restricted by the amount of the error-tagged data available. Fi- nally, the source article of the writer can be used in training as a feature, in the exact same way as with the models trained on error-tagged data, providing knowledge about which confusions are likely. Un- like article errors, preposition errors lend themselves very well to a study of confusion sets because the set of prepositions participating in the task is a lot big- ger than the set of article choices.

ネイティブのコーパスに人工的な誤りを混入させることで精度が上がった．
これは少数のアノテーションデータから多くの訓練事例を作成できる．

###ESL Data
####Preposition errors of ESL Data
>Preposition errors are one of the most common mis- takes that non-native speakers make. In the Cam- bridge Learner Corpus2 (CLC), which contains data by learners of different first language backgrounds and different proficiency levels, preposition errors account for about 13.5% of all errors and occur on average in 10% of all sentences (Leacock et al., 2010). Similar error rates have been reported for other annotated ESL corpora, e.g. (Izumi et al., 2003; Rozovskaya and Roth, 2010a; Tetreault et al., 2010). Learning correct preposition usage in En- glish is challenging for learners of all first language backgrounds (Dalgish, 1985; Bitchener et al., 2005; Gamon, 2010; Leacock et al., 2010).

CLCでも前置詞誤りは多い！

####実際に使ったデータについて
>The annotated data include sentences by speakers of five first language backgrounds: Chinese, Czech, Italian, Russian, and Spanish. The Czech, Italian, Russian and Spanish data come from the Interna- tional Corpus of Learner English (ICLE, (Granger et al., 2002)), which is a collection of essays writ- ten by advanced learners of English. The Chinese data is a part of the Chinese Learners of English cor- pus (CLEC, (Gui and Yang, 2003)) that contains es- says by students of all levels of proficiency. Table 1 shows preposition statistics based on the annotated data.

>The combined data include 4185 prepositions, 8.4% of which were judged to be incorrect by the annotators. Table 1 demonstrates that the error rates in the Chinese speaker data, for which different pro- ficiency levels are available, are 2 or 3 times higher than the error rates in other language groups. The data for other languages come from very advanced learners and, while there are also proficiency differnces among advanced speakers, their error rates are much lower.
We would also like to point out that we take as the baseline3 for the task the accuracy of the non- native data, or the proportion of prepositions used correctly. Using the error rate numbers shown in Table 1, the baseline for Chinese speakers is thus 84.9%, and for all the data combined it is 91.6%.

表の1は学習者のL1による前置詞誤り率の比較．
中国人では多いけれど，それは中国語L1のコーパスはCLEC，その他のL1のコーパスはICLEであり，学習者の習熟度が異なるからだと考えられる．

####L1による前置詞誤りの傾向
よく使われる10の前置詞について考察．

>We mentioned in Section 2 that not all preposition confusions are equally likely to occur and preposi- tion errors may depend on the first language of the writer. Han et al. (2010) show that preposition er- rors in the annotated corpus by Korean learners are not evenly distributed, some confusions occurring more often than others. We also observe that con- fusion frequencies differ by L1. This is consistent with other studies, which show that learners’ errors are influenced by their first language (Lee and Sen- eff, 2008; Leacock et al., 2010).

###Candidate setの改良
>We refer to the preposition originally chosen by the author in the non-native text as the source prepo- sition, and label denotes the correct preposition choice, as chosen by the annotator. Consider, for ex- ample, the following sentences from the annotated corpus.

例文
>We ate by*(source)/with(label) ourhands.

####L1dependent confusion sets
>L1ConfSet(from)= from, of, for 

こんな感じ．

####Enforcing L1-dependent Confusion Sets in Training
よくある学習時の状況設定

NegAll
>すべての候補のうち1つを正例として残りを負例にする．

NegL1

>Confusion setに含まれるものだけ考慮．正例は1つ．負例はのこりなのは一緒．  

ErrorL1

>人工的な前置詞誤りを含んで学習する方法．  

This method restricts the candidate set to L1ConfSet by generating artificial preposition errors in the spirit of Rozovskaya and Roth (2010b). The training data are thus no longer well-formed or clean, but augmented with L1 error statistics. 

ある前置詞を適当な確率で他の前置詞で置き換えることで，人工的なエラーを付与．


####テスト時の制約付与  

>To reduce the number of false alarms, correction systems generally use a threshold on the confidence of the classifier, following (Carlson et al., 2001), and propose a correction only when the confidence of the classifier is above the threshold. We show in Section 5 that the system trained on data with artificial errors performs competitively even without a thresh- old. The other systems use a threshold. We consider two ways of applying a threshold :

普通交差検定などを用いて，訓練セット+開発セットでパラメータ（しきい値）を決めていく．．．そういうこと？？

###Experimental setup
4つの実験設定 (training)

識別モデルで．．．
>In each training paradigm, we follow a discrimi- native approach, using an online learning paradigm and making use of the Averaged Perceptron Algo- rithm (Freund and Schapire, 1999) – we use the regularized version in Learning Based Java (LBJ, (Rizzolo and Roth, 2007)).  
While classical Per- ceptron comes with generalization bound related to the margin of the data, Averaged Perceptron also comes with a PAC-like generalization bound (Fre- und and Schapire, 1999). This linear learning al- gorithm is known, both theoretically and experi- mentally, to be among the best linear learning ap- proaches and is competitive with SVM and Logistic regression while being more efficient in training. It also has been shown to produce state-of-the-art results on many natural language applications (Pun- yakanok et al., 2008).

###Results and Discussion
結果！
>For each source lan- guage, the methods that restrict candidate sets in training or testing outperform the baseline system NegAll-Clean-ThreshAll that does not restrict can- didate sets. The NegAll-ErrorL1-NoThresh system performs better than the other three systems for all languages, except for Italian. In fact, for the Czech speaker data, all systems other than NegAll-ErrorL1- NoThresh, have a precision and a recall of 0, since no errors are detected10 .

提案手法（エラー混入）はItalian以外では良い結果．
>エラー混入の際はユニグラム．訂正は周辺も使えるので．．．イタリア語のデータがいろいろ何かあったのかも？？
ということはもともとのほうが良いのだから，イタリア語のデータの学習者は質がよかったのか？

システムの比較
>micro and macro average

…

>I got somehow lost!!!

>The differences between NegAll-ErrorL1-ThreshL1 and each of the other three systems are statistically significant. The table also demon- strates that the results on the correction task may vary widely. For example, the recall varies by lan- guage between 10.47% and 27.43% for the NegAll- ErrorL1-NoThresh system. The highest recall num- bers are obtained for Chinese speakers. These speakers also have the highest error rate, as we noted in Section 3.

ここでのRecall
>直さなくちゃいけないところを何個直せるか，というのがRecallに関わる．

検出と訂正をどちらもやっているので
>間違いがある．全部見つけられて直せたらR=100％ 

tp: 間違いを検出訂正できた
tn: 間違いを検出訂正できなかった
fp: 間違いじゃないのに間違いと検出した
fn: 間違いなので検出しなかった  
False alarmはこのような誤り訂正などのタスクで重要なので要確認．

>PR以外の指標とか，うまくバランスを取るようなぐらいに．．．設定したいな〜

>例文とnbestの候補が出るような提案はユーザーにとっては有用かも．

>コーパスから例文を出したり．．．できる