# Counting lines of a file
```sh 
cat file.txt | wc -l
```

# Splitting large file into pieces
##Exapmle 
the file contains 1k lines.
```sh
split -l 100 -d --verbose test.txt prefix_of_pieces
```
This will split `test.txt` into 10 piceces named `prefix_of_pieces00`,...,`prefix_of_pieces09`  
The option `-d` is for choosing *numeric* suffix instead of alphabetical suffix.

# Rename multiple files 
## Solution 1
If there are files named `text_xx` in the cwd, and one wants to change the name of those to `test_xx.txt`
```sh
find ./ -name "text_*" | xargs -i% mv % %.txt
```

## Solution 2
Using **GNU Parallel**, and if the originals are `test_xx.dat`, 
```sh
parallel -v mv "{} {.}.txt" ::: ./*.dat
```

# Parallel
## traditional: `xargs`
```sh
find /nwork/yu-s/aclweb.txt/anthology-new/ -name "*.txt" | xargs -i% echo /nwork/yu-s/Research/tools/fanseparser/parse.sh %.cleaned %.parsed > commandlist
```

## easy and modern: `parallell` (GNU Parallel)
```sh
parallel -j 30% -v ./senna-linux64 "< {} > {.}.senna" ::: /work/yu-s/cl/nldata/EnglishGigaWord/raw/apw_eng/*.cleaned
```


## GXP
```sh
will be written soon...
```



# GNU Parallel
##Examples
###Copying from one directory to another, using the basename
```sh
parallel -j 50% cp "{} ./fce/{/}" ::: ./fcetmp/dataset/*/*.xml
```

`{/}` means basename of globbed files


#find, grep
## Recursively find files contains a criteria 
```sh
find ../fce-released-dataset/dataset/ -type f -print0 | xargs -0 grep -il £ 
```


#Git
## Branching
```sh
git checkout -b new_branch
```
This is a synonym of 
```sh
git branch new_branch  
git checkout new_branch
```

### Pushing to Github
```sh
git push origin new_branch
```

### Merge to the master
```sh
git checkout master
git merge new_branch
```

### Deleting a branch
```sh
git branch -d new_branch
git push origin :new_branch
```

### When conflicts occurred...
```sh
# on master branch...
git merge dev 
warning: Cannot merge binary files: hoge (HEAD vs. dev)

Auto-merging hoge
CONFLICT (content): Merge conflict in hoge
Automatic merge failed; fix conflicts and then commit the result.

# original hoge on master branch is remaining untouched
# use git checkout --theirs path_to_file
git checkout --theirs hoge
```